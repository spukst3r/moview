#! /usr/bin/env python

from gi.repository import Gtk
from moview.config import Config
from moview.defs import DATA_DIR
from moview.mainwindow import MainWindow

if __name__ == '__main__':
    config = Config()
    MainWindow(config).run()

