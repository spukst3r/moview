import re
import urllib2

from pymongo import Connection, errors

class Movies:
    def __init__(self, config):
        assert(config.has_key('mongo_db') and config.has_key('mongo_host'))

        args = { 'host': config['mongo_host'] }

        if config.has_key('mongo_port'):
            args['port'] = config['mongo_port']

        try:
            connection = Connection(**args)
        except errors.AutoReconnect, e:
            raise RuntimeError('Failed to connect to mongo server \'{}\':\n{}'
                    .format(config['mongo_host'], e.message))

        self.db = connection[config['mongo_db']]

    def movies(self, filter_string=''):
        movies = self.db.movies

        regex = re.compile(filter_string, re.IGNORECASE)

        if filter_string:
            cursor = movies.find({'$or': [
                    {'Title': regex},
                    {'Director': regex},
                    {'Actors': regex}
                ]},
                {'Title': 1}).sort('Title')
        else:
            cursor = movies.find({}, {'Title': 1}).sort('Title')

        for m in cursor:
            yield m

    def get_movie_info(self, oid):
        movies = self.db.movies

        return movies.find_one({'_id': oid})

