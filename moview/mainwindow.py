import os
import sys

from gi.repository import Gtk, Pango
from moview.defs import DATA_DIR
from moview.db import Movies

def _(s):
    return s

class MainWindow:
    def __init__(self, config):
        self.builder = Gtk.Builder()

        filename = os.path.join(DATA_DIR, 'ui.glade')

        assert(os.path.exists(filename))

        try:
            self.movies = Movies(config)
        except RuntimeError, e:
            print e.message
            sys.exit()

        self.builder.add_from_file(filename)

        self.ui_setup()
        self.tv_setup()
        self.fetch_data()

        self.last_updated_movie = None
        self.config = config

    def tv_setup(self):
        title_column = self.builder.get_object('column_title')
        icon_column = self.builder.get_object('column_icon')

        cell_title = Gtk.CellRendererText()
        cell_icon = Gtk.CellRendererPixbuf()

        title_column.pack_end(cell_title, True)
        icon_column.pack_end(cell_icon, True)

        title_column.add_attribute(cell_title, 'text', 0)
        icon_column.add_attribute(cell_icon, 'pixbuf', 1)

    def fetch_data(self, filter_string=''):
        tree_store = self.builder.get_object('tree_store')

        tree_store.clear()

        for m in self.movies.movies(filter_string):
            it = tree_store.append(None, [m['Title'], None, m['_id']])

    def ui_setup(self):
        main_window = self.builder.get_object('main_window')

        main_window.connect('destroy', Gtk.main_quit)
        main_window.set_title('Moview')
        main_window.show_all()

        toolbar = self.builder.get_object('toolbar')
        toolbar.get_style_context().add_class(Gtk.STYLE_CLASS_PRIMARY_TOOLBAR)

        tree_view = self.builder.get_object('movie_list')
        tree_view.connect('cursor-changed', self.show_info)

        search_entry = self.builder.get_object('search-entry')
        search_entry.connect('changed', self.filter_movies)

    def filter_movies(self, widget, data=None):
        self.fetch_data(widget.get_text())

        tree_view = self.builder.get_object('movie_list')
        path = tree_view.get_path_at_pos(1, 1)

        #tree_view.set_cursor(path)

    def show_info(self, widget, data=None):
        sel = widget.get_selection()

        if sel:
            model, it = sel.get_selected()

            mid =  model.get_value(it, 2)

            if self.last_updated_movie != mid:
                self.last_updated_movie = mid

                info = self.movies.get_movie_info(mid)
                buf = self.get_textbuf(info)

                text_view = self.builder.get_object('movie_info')
                text_view.set_buffer(buf)

    def get_textbuf(self, info):
        buf = Gtk.TextBuffer()
        keys = self.config['layout']

        table = buf.get_tag_table()
        it = buf.get_start_iter()

        for k, lk, text_attr in keys:
            if lk:
                buf.insert(it, lk + u': ')

            try:
                s = info[k]
            except KeyError, e:
                print e.message
                continue

            s += u'\n'
            if text_attr:
                tag = Gtk.TextTag()
                tag.set_property('font', text_attr)
                table.add(tag)

                buf.insert_with_tags(it, s, tag)
            else:
                buf.insert(it, s)

            it = buf.get_end_iter()

        if info.has_key('poster_img'):
            pass
        else:
            pass

        return buf

    def run(self):
        Gtk.main()

