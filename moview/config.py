import json
import os

def _(s):
    return s

config_path = os.path.expanduser('~') + '/.config/moview/config.json'
default_config = {
        'collection_path': '/mnt/data',
        'mongo_host': 'localhost',
        'mongo_db': 'moview',
        'player': None,
        'layout': [ ('Title', None, 'Bold 18'),
                ('Director', _('Director'), None),
                ('Year', _('Year'), None),
                ('Genre', _('Genre'), None),
                ('imdbRating', _('IMDB Rating'), 'Bold 14'),
                ('Actors', _('Actors'), None),
                ('Rated', _('Rated'), 'Bold'),
                ('Plot', _('Plot'), None),
                ]

        }

class Config:
    def __init__(self):
        self.config = {}
        self.__load_config()

    def __getitem__(self, key, default=None):
        return self.config.get(key, default)

    def __setitem__(self, key, value):
        self.config[key] = value

    def has_key(self, key):
        return self.config.has_key(key)

    def __load_config(self):
        dirname = os.path.dirname(config_path)

        if not os.path.exists(config_path):
            if not os.path.exists(dirname):
                os.makedirs(dirname)
            self.__default_config()

        cnf = open(config_path)
        self.config = json.load(cnf)
        cnf.close()

    def __default_config(self):
        cnf = open(config_path, 'w')
        json.dump(default_config, cnf, sort_keys=True, indent=2)
        cnf.close()

    def save(self):
        cnf = open(config_path, 'w')
        json.dump(self.config, cnf, sort_keys=True, indent=2)

